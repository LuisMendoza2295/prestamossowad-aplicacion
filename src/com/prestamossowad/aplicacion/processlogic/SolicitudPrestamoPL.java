package com.prestamossowad.aplicacion.processlogic;

import com.prestamossowad.aplicacion.generics.InsertFacade;
import com.prestamossowad.aplicacion.generics.QueryFacade;
import com.prestamossowad.aplicacion.generics.UpdateFacade;
import com.prestamossowad.dominio.Prestamo;
import com.prestamossowad.dominio.SolicitudPrestamo;
import com.prestamossowad.dominio.Usuario;

public class SolicitudPrestamoPL {

	private static SolicitudPrestamoPL instance = null;

    private SolicitudPrestamoPL() {
    }

    public static SolicitudPrestamoPL getInstance() {
        if (instance == null) {
            instance = new SolicitudPrestamoPL();
        }

        return instance;
    }
    
    public String registrarSolicitudPrestamo(SolicitudPrestamo objSolicitudPrestamo, Usuario objUsuarioPrestamista, Usuario objUsuarioPrestatario, int idTipoInteres, int idMoneda){
    	//objSolicitudPrestamo.setObjTasaInteres(QueryFacade.getInstance().getTipoInteres(idTipoInteres).getObjTasaInteres());
    	objSolicitudPrestamo.setObjTasaInteres(QueryFacade.getInstance().getTasaInteres(QueryFacade.getInstance().getTipoInteres(idTipoInteres)));
    	objSolicitudPrestamo.setObjMoneda(QueryFacade.getInstance().getMoneda(idMoneda));
    	return InsertFacade.getInstance().insertSolicitudPrestamo(objSolicitudPrestamo, objUsuarioPrestamista, objUsuarioPrestatario);
    }

    public String reenviarSolicitud(SolicitudPrestamo objSolicitudPrestamo, Usuario objUsuario, int idTipoInteres, int idMoneda) {
        String resultMessage;

        try {
            if (objUsuario.getId() == objSolicitudPrestamo.getObjUsuarioPrestamista().getId()) {
                objSolicitudPrestamo.setAprobadoPrestatario(false);
                objSolicitudPrestamo.setAprobadoPrestamista(true);
            } else if (objUsuario.getId() == objSolicitudPrestamo.getObjUsuarioPrestatario().getId()) {
                objSolicitudPrestamo.setAprobadoPrestamista(false);
                objSolicitudPrestamo.setAprobadoPrestatario(true);
            }

            objSolicitudPrestamo.setObjTasaInteres(QueryFacade.getInstance().getTasaInteres(QueryFacade.getInstance().getTipoInteres(idTipoInteres)));
            objSolicitudPrestamo.setObjMoneda(QueryFacade.getInstance().getMoneda(idMoneda));
            objSolicitudPrestamo.setObjUsuarioModificacion(objUsuario);

            boolean result = UpdateFacade.getInstance().updateSolicitudPrestamo(objSolicitudPrestamo);

            if (result) {
                resultMessage = "Solicitud reenviada!";
            } else {
                resultMessage = "No se pudo realizar el reenvio";
            }
        } catch (Exception e) {
            e.printStackTrace();
            resultMessage = "Error en el Registro";
        }

        return resultMessage;
    }

    public String aceptarSolicitud(SolicitudPrestamo objSolicitudPrestamo, Usuario objUsuario, int idTipoInteres, int idMoneda) {
        String resultMessage;

        try {
            if (objUsuario.getId() == objSolicitudPrestamo.getObjUsuarioPrestamista().getId()) {
                objSolicitudPrestamo.setAprobadoPrestamista(true);
            } else if (objUsuario.getId() == objSolicitudPrestamo.getObjUsuarioPrestatario().getId()) {
                objSolicitudPrestamo.setAprobadoPrestatario(true);
            }
            
            objSolicitudPrestamo.setObjTasaInteres(QueryFacade.getInstance().getTasaInteres(QueryFacade.getInstance().getTipoInteres(idTipoInteres)));
            objSolicitudPrestamo.setObjMoneda(QueryFacade.getInstance().getMoneda(idMoneda));
            objSolicitudPrestamo.setObjUsuarioModificacion(objUsuario);

            boolean result = UpdateFacade.getInstance().updateSolicitudPrestamo(objSolicitudPrestamo);

            if (result) {
                resultMessage = "Solicitud aceptada!";
                
                if(objSolicitudPrestamo.isAprobadoPrestamista() && objSolicitudPrestamo.isAprobadoPrestatario()){
                    Prestamo objPrestamo = objSolicitudPrestamo.crearPrestamo();
                    
                    boolean insertPrestamo = InsertFacade.getInstance().insertPrestamo(objPrestamo);
                    if(insertPrestamo){
                        insertPrestamo = InsertFacade.getInstance().insertCuotas(objPrestamo);
                    }
                    
                    if(insertPrestamo){
                        resultMessage = resultMessage.concat("\nPrestamo registrado!");
                    }else{
                        resultMessage = resultMessage.concat("\nNo se pudo registrar el prestamo");
                    }
                }
            } else {
                resultMessage = "No se pudo realzar la aceptacion";
            }
        } catch (Exception e) {
            e.printStackTrace();
            resultMessage = "Error en el Registro";
        }

        return resultMessage;
    }
    
    /*public boolean agregarTiposPago(List<TipoPago> lstTipoPagos, SolicitudPrestamo objSolicitudPrestamo){
        boolean resultado = false;
        
        for(TipoPago objTipoPago : lstTipoPagos){
            DAOTipoPago.getInstance().insert(objTipoPago);
        }
        objSolicitudPrestamo.setLstTiposPago(lstTipoPagos);
        
        return resultado;
    }*/
}
