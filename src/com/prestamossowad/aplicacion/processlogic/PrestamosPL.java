package com.prestamossowad.aplicacion.processlogic;

import com.prestamossowad.dao.concrete.DAOCuota;
import com.prestamossowad.dominio.Cuota;

public class PrestamosPL {

	private static PrestamosPL instance = null;
	
	private PrestamosPL(){
	}
	
	public static PrestamosPL getInstance(){
		if(instance == null){
			instance = new PrestamosPL();
		}
		
		return instance;
	}
	
	public String pagarCuota(Cuota objCuota){
		String message = "";
		
		try{
			message = DAOCuota.getInstance().update(objCuota);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return message;
	}
}
