package com.prestamossowad.aplicacion.generics;

import java.util.ArrayList;
import java.util.List;

import com.prestamossowad.dao.concrete.DAOCuota;
import com.prestamossowad.dao.concrete.DAOMoneda;
import com.prestamossowad.dao.concrete.DAOPrestamo;
import com.prestamossowad.dao.concrete.DAOSolicitudPrestamo;
import com.prestamossowad.dao.concrete.DAOTasaInteres;
import com.prestamossowad.dao.concrete.DAOTipoInteres;
import com.prestamossowad.dao.concrete.DAOTipoPago;
import com.prestamossowad.dao.concrete.DAOUsuario;
import com.prestamossowad.dominio.Cuota;
import com.prestamossowad.dominio.Moneda;
import com.prestamossowad.dominio.Prestamo;
import com.prestamossowad.dominio.SolicitudPrestamo;
import com.prestamossowad.dominio.TasaInteres;
import com.prestamossowad.dominio.TipoInteres;
import com.prestamossowad.dominio.TipoPago;
import com.prestamossowad.dominio.Usuario;

public class QueryFacade {

	private static QueryFacade instance = null;

    private QueryFacade() {
    }

    public static QueryFacade getInstance() {
        if (instance == null) {
            instance = new QueryFacade();
        }

        return instance;
    }

    public List<Moneda> getAllMonedas() {
        return DAOMoneda.getInstance().getAllMonedas();
    }

    public Moneda getMoneda(int id) {
        return DAOMoneda.getInstance().getMoneda(id);
    }
    
    public Usuario getUsuario(int id) {
        return DAOUsuario.getInstance().getUsuario(id);
    }
    
    public TasaInteres getTasaInteres(int id) {
        return DAOTasaInteres.getInstance().getTasaInteres(id);
    }

    public List<TipoPago> getAllTiposPago(List<Integer> lstIdsTiposPago) {
        List<TipoPago> lstTiposPago = new ArrayList<>();

        for (Integer id : lstIdsTiposPago) {
            lstTiposPago.add(this.getTipoPago(id));
        }

        return lstTiposPago;
    }

    public TipoPago getTipoPago(int id) {
        return DAOTipoPago.getInstance().getTipoPago(id);
    }

    public SolicitudPrestamo getSolicitudPrestamo(int id) {
        return DAOSolicitudPrestamo.getInstance().getSolicitudPrestamo(id);
    }

    public List<SolicitudPrestamo> getAllSolicitudesPrestamoPendientesAprobarPrestamistaAsPrestamista(Usuario objUsuario) {
        return DAOSolicitudPrestamo.getInstance().getAllSolicitudesPrestamoPendientesAprobarPrestamistaAsPrestamista(objUsuario);
    }

    public List<SolicitudPrestamo> getAllSolicitudesPrestamoPendientesAprobarPrestatarioAsPrestamista(Usuario objUsuario) {
        return DAOSolicitudPrestamo.getInstance().getAllSolicitudesPrestamoPendientesAprobarPrestatarioAsPrestamista(objUsuario);
    }
    
    public List<SolicitudPrestamo> getAllSolicitudesPrestamoPendientesAprobarPrestamistaAsPrestatario(Usuario objUsuario){
        return DAOSolicitudPrestamo.getInstance().getAllSolicitudesPrestamoPendientesAprobarPrestamistaAsPrestatario(objUsuario);
    }
    
    public List<SolicitudPrestamo> getAllSolicitudesPrestamoPendientesAprobarPrestatarioAsPrestatario(Usuario objUsuario){
        return DAOSolicitudPrestamo.getInstance().getAllSolicitudesPrestamoPendientesAprobarPrestatarioAsPrestatario(objUsuario);
    }
    
    public List<TipoInteres> getAllTiposInteres(Usuario objUsuario){
    	return DAOTipoInteres.getInstance().getAllTiposInteres(objUsuario);
    }
    
    public List<Usuario> getAllUsuarios(Usuario objUsuario){
    	return DAOUsuario.getInstance().getAllUsuarios(objUsuario);
    }
    
    public TipoInteres getTipoInteres(int id){
    	return DAOTipoInteres.getInstance().getTipoInteres(id);
    }
    
    public List<Prestamo> getAllPrestamosActivosAsPrestamista(Usuario objUsuario){
    	return DAOPrestamo.getInstance().getAllPrestamosActivos(objUsuario, true);
    }
    
    public List<Prestamo> getAllPrestamosActivosAsPrestatario(Usuario objUsuario){
    	return DAOPrestamo.getInstance().getAllPrestamosActivos(objUsuario, false);
    }
    
    public Prestamo getPrestamo(int id){
    	return DAOPrestamo.getInstance().getPrestamo(id);
    }
    
    public Cuota getCuota(int id){
    	return DAOCuota.getInstance().getCuota(id);
    }
    
    public Prestamo getPrestamo(Cuota objCuota){
    	return DAOPrestamo.getInstance().getPrestamo(objCuota);
    }
    
    public TasaInteres getTasaInteres(TipoInteres objTipoInteres){
    	return DAOTasaInteres.getInstance().getTasaInteres(objTipoInteres);
    }
    
    public List<TasaInteres> getAllTasasInteres(Usuario objUsuario){
    	return DAOTasaInteres.getInstance().getAllTasasInteres(objUsuario);
    }
}
