package com.prestamossowad.aplicacion.generics;

import com.prestamossowad.dao.concrete.DAOUsuario;
import com.prestamossowad.dominio.Usuario;

public class SecurityFacade {

	private static SecurityFacade instance = null;
	
	private SecurityFacade(){
	}
	
	public static SecurityFacade getInstance(){
		if(instance == null){
			instance = new SecurityFacade();
		}
		
		return instance;
	}
	
	public Usuario verifyAccess(String username, String password){
		return DAOUsuario.getInstance().getUsuario(username, password);
	}
}
