package com.prestamossowad.aplicacion.generics;

import com.prestamossowad.dao.concrete.DAODetalleTipoPago;
import com.prestamossowad.dao.concrete.DAOTipoInteres;
import com.prestamossowad.dominio.SolicitudPrestamo;
import com.prestamossowad.dominio.TipoInteres;

public class DeleteFacade {

private static DeleteFacade instance = null;
    
    private DeleteFacade(){
    }
    
    public static DeleteFacade getInstance(){
        if(instance == null){
            instance = new DeleteFacade();
        }
        
        return instance;
    }
    
    public boolean deleteDetalleTiposPago(SolicitudPrestamo objSolicitudPrestamo){
        return DAODetalleTipoPago.getInstance().delete(objSolicitudPrestamo);
    }
    
    public boolean deleteTipoInteres(TipoInteres objTipoInteres){
    	return DAOTipoInteres.getInstance().delete(objTipoInteres);
    }
}
