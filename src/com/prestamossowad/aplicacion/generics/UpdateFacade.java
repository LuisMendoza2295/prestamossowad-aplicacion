package com.prestamossowad.aplicacion.generics;

import com.prestamossowad.dao.concrete.DAOSolicitudPrestamo;
import com.prestamossowad.dominio.SolicitudPrestamo;

public class UpdateFacade {

private static UpdateFacade instance = null;
    
    private UpdateFacade(){
    }
    
    public static UpdateFacade getInstance(){
        if(instance == null){
            instance = new UpdateFacade();
        }
        
        return instance;
    }
    
    public boolean updateSolicitudPrestamo(SolicitudPrestamo objSolicitudPrestamo){
        return DAOSolicitudPrestamo.getInstance().update(objSolicitudPrestamo);
    }
}
