package com.prestamossowad.aplicacion.generics;

import java.util.List;

import com.prestamossowad.dao.concrete.DAOComprobante;
import com.prestamossowad.dao.concrete.DAOCuota;
import com.prestamossowad.dao.concrete.DAODetalleSolicitudPrestamo;
import com.prestamossowad.dao.concrete.DAODetalleTipoPago;
import com.prestamossowad.dao.concrete.DAOPago;
import com.prestamossowad.dao.concrete.DAOPrestamo;
import com.prestamossowad.dao.concrete.DAOSolicitudPrestamo;
import com.prestamossowad.dao.concrete.DAOTasaInteres;
import com.prestamossowad.dao.concrete.DAOTipoInteres;
import com.prestamossowad.dao.concrete.DAOTipoPago;
import com.prestamossowad.dao.concrete.DAOUsuario;
import com.prestamossowad.dominio.Comprobante;
import com.prestamossowad.dominio.DetalleSolicitudPrestamo;
import com.prestamossowad.dominio.Pago;
import com.prestamossowad.dominio.Prestamo;
import com.prestamossowad.dominio.SolicitudPrestamo;
import com.prestamossowad.dominio.TasaInteres;
import com.prestamossowad.dominio.TipoInteres;
import com.prestamossowad.dominio.TipoPago;
import com.prestamossowad.dominio.Usuario;

public class InsertFacade {

private static InsertFacade instance = null;
    
    private InsertFacade(){
    }
    
    public static InsertFacade getInstance(){
        if(instance == null){
            instance = new InsertFacade();
        }
        
        return instance;
    }
    
    public String insertSolicitudPrestamo(SolicitudPrestamo objSolicitudPrestamo, Usuario objUsuarioPrestamista, Usuario objUsuarioPrestatario){
    	return DAOSolicitudPrestamo.getInstance().insert(objSolicitudPrestamo, objUsuarioPrestamista, objUsuarioPrestatario);
    }
    
    public String insertUsuario(Usuario objUsuario){
    	return DAOUsuario.getInstance().insert(objUsuario);
    }
    
    public boolean insertDetalleTipoPago(SolicitudPrestamo objSolicitudPrestamo){
        return DAODetalleTipoPago.getInstance().insertAllDetalleTipoPago(objSolicitudPrestamo);
    }
    
    public boolean insertPrestamo(Prestamo objPrestamo){
        return DAOPrestamo.getInstance().insert(objPrestamo);
    }
    
    public boolean insertCuotas(Prestamo objPrestamo){
        return DAOCuota.getInstance().insert(objPrestamo);
    }
    
    public boolean insertTipoPago(TipoPago objTipoPago){
        return DAOTipoPago.getInstance().insert(objTipoPago);
    }
    
    public boolean insertPago(Pago objPago){
        return DAOPago.getInstance().insert(objPago);
    }
    
    public boolean insertComprobante(Comprobante objComprobante){
        return DAOComprobante.getInstance().insert(objComprobante);
    }
    
    public boolean insertDetalleSolicitudPrestamo(List<DetalleSolicitudPrestamo> lstDetalleSolicitudPrestamo){
        return DAODetalleSolicitudPrestamo.getInstance().insertDetalleSolicitudPrestamo(lstDetalleSolicitudPrestamo);
    }
    
    public boolean insertTasaInteres(TipoInteres objTipoInteres, TasaInteres objTasaInteres){
    	boolean result = DAOTasaInteres.getInstance().insert(objTasaInteres);
    	
    	return result;
    }
    
    public boolean insertTipoInteres(TasaInteres objTasaInteres, Usuario objUsuario){
    	return DAOTipoInteres.getInstance().insert(objTasaInteres, objUsuario);
    }
}
